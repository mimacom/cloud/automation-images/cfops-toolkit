FROM ubuntu:bionic

# Useful utilities
RUN apt-get update && apt-get -y install \
        curl \
        dnsutils \
        git \
        jq \
        unzip \
        wget && \
    rm -rf /var/lib/apt/lists/*

# install dependencies
RUN apt-get update
RUN apt-get install -y build-essential zlibc zlib1g-dev ruby ruby-dev openssl libxslt1-dev libxml2-dev libssl-dev libreadline7 libreadline-dev libyaml-dev libsqlite3-dev sqlite3

# configure versions
ENV CF_CLI_VERSION "6.44.1"
ENV BOSH_CLI_VERSION "5.5.0"
ENV BBL_CLI_VERSION "7.6.0"
ENV TERRAFORM_VERSION "0.11.13"

# Install BOSH
RUN set -e; \
    curl -L "https://github.com/cloudfoundry/bosh-cli/releases/download/v${BOSH_CLI_VERSION}/bosh-cli-${BOSH_CLI_VERSION}-linux-amd64" > /usr/local/bin/bosh
RUN chmod +x /usr/local/bin/bosh
RUN bosh --version

# Install BBL
RUN set -e; \
    curl -L "https://github.com/cloudfoundry/bosh-bootloader/releases/download/v7.6.0/bbl-v7.6.0_linux_x86-64" > /usr/local/bin/bbl
RUN chmod +x /usr/local/bin/bbl
RUN bbl --version

# Install Terraform
RUN curl -L "https://releases.hashicorp.com/terraform/0.11.13/terraform_0.11.13_linux_amd64.zip" > /tmp/terraform.zip
RUN cd /tmp > /dev/null && unzip ./terraform.zip && mv ./terraform /usr/local/bin/terraform && rm terraform.zip
RUN chmod +x /usr/local/bin/terraform
RUN terraform --version

# Install CF CLI: https://github.com/cloudfoundry/cli
RUN set -e; \
    curl -L "https://packages.cloudfoundry.org/stable?release=linux64-binary&version=${CF_CLI_VERSION}&source=github-rel" | tar -zx -C /usr/local/bin;
RUN cf --version
